import java.util.Scanner;
public class TheTaskNum5 {
    public static void main(String[] args) {

        Scanner scannerObj = new Scanner(System.in);
        System.out.println("Enter digit: ");
        int number=1;
        if (scannerObj.hasNextInt()) {
            number = scannerObj.nextInt();
        }
            if (number == 0) {
                System.out.println("Hello World!");
            } else if ((number % 2) == 0) {
                System.out.println(number + 1);
            } else {
                System.out.println(number - 3);
            }

        }
    }
