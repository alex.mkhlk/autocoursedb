import java.util.Scanner;

public class TheTaskNum6 {

    public static void main(String[] args) {

        Scanner scannerObj = new Scanner(System.in);
        System.out.println("Enter digit: ");
        int num;
        num = scannerObj.nextInt();

        for(int i = 1; i <= 10; i++){
            {
                System.out.println(i + " * " + num + " = " + num * i);
            }
        }


    }
}
