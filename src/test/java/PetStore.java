public class PetStore {
    public String Animal;
    public String Gender;
    public double MalePrice;
    public double FemalePrice;
    public int AnimalsCount;

    public PetStore(String animal, String gender, double malePrice, double femalePrice, int AnimalsCount) {
        this.Animal = animal;
        this.Gender = gender;
        this.MalePrice = malePrice;
        this.FemalePrice = femalePrice;
        this.AnimalsCount = AnimalsCount;
    }

    public double CalcPrice(String gender, int count) {
        double price = 0;
        if (count > AnimalsCount) {
            System.out.println("Not enough animals in the store");
        }

        switch (gender) {
            case "male":
                price = MalePrice * count;
                break;
            case "female":
                price = FemalePrice * count;
                break;
            default:
                System.out.println("Incorrect gender value");
                break;

        }
        return price;
    }
    public static void main(String[] args) {
        PetStore DogStore = new PetStore("dog", "male", 5.7, 7.8, 30);
        double priceOfDogs = DogStore.CalcPrice("male", 3);
        System.out.println(priceOfDogs);

        PetStore WolfStore = new PetStore("wolf","female",4.4,45,12);
        double priceOfWolf = WolfStore.CalcPrice("male",5);
        System.out.println(priceOfWolf);

        PetStore PenguinStore = new PetStore("penguin", "female",12.3,10.5,55);
        double priceOfPenguin = PenguinStore.CalcPrice("-",3);
    }
}
