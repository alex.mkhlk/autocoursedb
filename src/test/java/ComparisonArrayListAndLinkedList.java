import java.util.ArrayList;
import java.util.LinkedList;

public class ComparisonArrayListAndLinkedList {
    public static void main(String[] args) {

        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        LinkedList<Integer> linkedList = new LinkedList<Integer>();

        for (int q = 0; q < 1000000; q++){
            arrayList.add(q);
            linkedList.add(q);
        }

        long arrayListTime = System.currentTimeMillis();
        arrayList.add(1000, 777);
        long arrayResultTime = System.currentTimeMillis() - arrayListTime;
        System.out.println("The result of adding for ArrayList = " + arrayResultTime + "ms");

        long linkedListTime = System.currentTimeMillis();
        linkedList.add(1000, 777);
        long linkedResultTime = System.currentTimeMillis() - linkedListTime;
        System.out.println("The result of adding for LinkedList = " + linkedListTime + "ms");
        System.out.println();

        long arrayListTimee = System.currentTimeMillis();
        arrayList.remove(1001);
        long arrayResultTimee = System.currentTimeMillis() - arrayListTimee;
        System.out.println("The result of removing for ArrayList = " + arrayResultTimee + "ms");

        long linkedListTimee = System.currentTimeMillis();
        linkedList.remove(1001);
        long linkedResultTimee = System.currentTimeMillis() - linkedListTimee;
        System.out.println("The result of removing for LinkedList = " + linkedListTimee + "ms");
        System.out.println();

        long arrayListTimeee = System.currentTimeMillis();
        arrayList.get(10000);
        long arrayResultTimeee = System.currentTimeMillis() - arrayListTimeee;
        System.out.println("The result of getting for ArrayList = " + arrayResultTimeee + "ms");

        long linkedListTimeee = System.currentTimeMillis();
        linkedList.get(10000);
        long linkedResultTimeee = System.currentTimeMillis() - linkedListTimeee;
        System.out.println("The result of getting for LinkedList = " + linkedListTimeee + "ms");


    }
}
