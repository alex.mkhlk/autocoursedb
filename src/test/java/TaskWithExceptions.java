import java.util.InputMismatchException;
import java.util.Scanner;

public class TaskWithExceptions {
    public static void main(String[] args)  {

        try {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the first digit: ");
        int digit1 = scanner.nextInt();
        if (digit1 == 0) throw new Exception("Введенное Вами число выходит за рамки допустимых значений");
        System.out.println("Enter the second digit: ");
        int digit2 = scanner.nextInt();

            double result = digit1 / digit2;
            System.out.println("Result: " + result);

        }
        catch (ArithmeticException DivisionByZero) {
            System.out.println("Введенное Вами число выходит за рамки допустимых значений");
        } catch (InputMismatchException Mismatch) {
            System.out.println("Введенное Вами значение не является числом");
        } catch (Exception firstDigitIsZewo) {
            System.out.println("Введенное Вами число выходит за рамки допустимых значений");
        }
    }
}
