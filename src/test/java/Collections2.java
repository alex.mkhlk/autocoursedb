import java.util.ArrayList;
public class Collections2 {
    public static void main(String[] args) {

        ArrayList<String> CollectionName = new ArrayList<String>();

        CollectionName.add("Aaron");
        CollectionName.add("Aron");
        CollectionName.add("Aaron");
        CollectionName.add("Fabian");
        CollectionName.add("Blake");
        CollectionName.add("Bennett");
        CollectionName.add("Blake");
        CollectionName.add("Harrison");
        CollectionName.add("Bret");
        CollectionName.add("Brett");
        CollectionName.add("Bill");
        CollectionName.add("Null");

        System.out.println("The entire list of names: " + CollectionName);

        Object[] deletingDublicateNames = CollectionName.toArray();
        for (Object collection : deletingDublicateNames) {
            if (CollectionName.indexOf(collection) != CollectionName.lastIndexOf(collection)) {
                CollectionName.remove(CollectionName.lastIndexOf(collection));
                System.out.println("Duplicate that will be removed: " + collection);
            }
        }
        System.out.println("The clean list without duplicates: " + CollectionName);
    }
}
