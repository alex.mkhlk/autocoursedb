import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class TheTaskNum11_Collections {
    public static void main(String[] args) {
        Scanner scannerObj = new Scanner(System.in);

        String Text = "In order to reach the heights our way of practicing life should be changed according to the current passion.\n " +
                "Keep going with changed habits suiting your surrounding and achieve success in your life.\n" +
                "Whatever work you are handling and whatever way you are dealing,\n" +
                "The belief you are going to put into it is going to fetch you success,\n" +
                "So believe in what you doing success will be yours automatically. Forward to automation. TEST";
        String[] ArrayOfText = Text.split("[\\s,.\\-!?]+");

        ArrayList<String> ListOfText = new ArrayList<String>(Arrays.asList(ArrayOfText));
        System.out.println("Total number of elements: " + ListOfText.size());
        System.out.println("Enter the index number of element:  ");
        int NumberInput = scannerObj.nextInt();

        System.out.println("Enter the direction through the collection \"up\" or \"down\":  ");
        String DirectionInput = scannerObj.next();

        if (NumberInput < 0 || NumberInput >= ListOfText.size()) {
            System.out.println("Please enter a number between 0 and " + ListOfText.size());
        } else {
            if (DirectionInput.equalsIgnoreCase("up")) {
                for (int i = NumberInput; i < ListOfText.size(); i++) {
                    System.out.println(ListOfText.get(i) + " ");
                }
            } else if (DirectionInput.equalsIgnoreCase("down")) {
                for (int i = NumberInput; i >= 0; i--) {
                    System.out.println(ListOfText.get(i) + " ");
                }
            } else {
                System.out.println("Input Error.Please enter the correct value.");
            }


        }
    }
}
