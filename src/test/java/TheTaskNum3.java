import java.util.Scanner;
public class TheTaskNum3 {

    public static void main(String[] args) {

        Scanner scannerObj = new Scanner(System.in);
        System.out.println("Enter two numbers separated by a space: ");

        String[][] twoDimensionalArray = {
                {"Sam", "Smith","Sergo","Ceed","Karl"},
                {"Robert", "Delgro","Tom","Sem","Tomsin"},
                {"James", "Gosling","Diego","Kachu","Diblo"},
                {"Sam", "Smith","Bree","Sonya","And"},
                {"Sa", "Smi","Avik","Hell","Roy"}
        };

        int keyboardInput = scannerObj.nextInt();
        int keyboardInput2 = scannerObj.nextInt();
        if (keyboardInput > twoDimensionalArray.length) {
            System.out.println("You are out of range. Try again!");
        }
        if (keyboardInput2 > twoDimensionalArray.length) {
            System.out.println("You are out of range. Try again!");
        }
        else {
            System.out.println(twoDimensionalArray[keyboardInput][keyboardInput2]);
        }
    }
}
