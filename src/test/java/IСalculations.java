interface IСalculations {
    double getPerimeter ();
    int getNumberOfCorners ();
}
