public class Hexagon implements IСalculations{
    double sideOne;
    double sideTwo;
    double sideThree;
    double sideFour;
    double sideFive;
    double sideSix;

    @Override
    public double getPerimeter() {
        double perimeterOfHexagon = sideOne+sideTwo+sideThree+sideFour+sideFive+sideSix;
        return perimeterOfHexagon;
    }

    @Override
    public int getNumberOfCorners() {
        return 6;
    }
}
